migrate:
	docker-compose run --rm app python3 manage.py migrate

makemigrations:
	docker-compose run --rm app python3 manage.py makemigrations

createsuperuser:
	docker-compose run --rm app python3 manage.py createsuperuser

collectstatic:
	docker-compose run --rm app python3 manage.py collectstatic --no-input

shell:
	docker-compose run --rm app python3 manage.py shell

debug:
	docker-compose run --rm app python3 manage.py debug

piplock:
	poetry install
	sudo chown -R ${USER} poetry.lock

lint:
	isort .
	black --config pyproject.toml .
	flake8 --config setup.cfg

check_lint:
	isort --check --diff .
	flake8 --config setup.cfg
	black --check --config pyproject.toml .

test:
	python ./src/manage.py makemigrations --check --dry-run
	python ./src/manage.py migrate
	pytest ./src/

app:
	python manage.py runserver 0.0.0.0:8000

bot:
	python manage.py run_bot

run_bot:
	docker-compose up -d bot --build

stop_bot:
	docker-compose stop bot

run_app:
	docker-compose up -d app --build

stop_app:
	docker-compose stop app


