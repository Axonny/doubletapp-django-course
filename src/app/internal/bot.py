from django.conf import settings
from telegram.ext import CommandHandler, MessageHandler, Updater, filters

from app.internal.transport.bot.handlers import handle_error, handlers, set_contact


def run_bot() -> None:
    updater = Updater(token=settings.TG_BOT_TOKEN, use_context=True)

    for name, handler in handlers.items():
        updater.dispatcher.add_handler(CommandHandler(name, handler))

    updater.dispatcher.add_error_handler(handle_error)
    updater.dispatcher.add_handler(MessageHandler(filters.Filters.contact, set_contact))

    updater.start_polling()
    # application.run_webhook(
    #     listen="0.0.0.0",
    #     port=8443,
    #     url_path=settings.TG_BOT_TOKEN,
    #     key=settings.KEY_PATH,
    #     cert=settings.CERT_PATH,
    #     webhook_url=settings.BASE_URL + settings.TG_BOT_TOKEN,
    # )

    updater.idle()
