import factory

from app.internal.models import BankAccount, Card, MoneyTransactionHistory, PersonalData


class PersonalDataFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = PersonalData

    id = factory.Faker("pyint")
    username = factory.Faker("first_name")
    phone = factory.Faker("phone_number")


class BankAccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = BankAccount

    number = factory.Faker("iban")
    owner = factory.SubFactory(PersonalDataFactory)


class CardFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Card

    number = factory.Faker("credit_card_number")
    expiry = factory.Faker("credit_card_expire")
    code = factory.Faker("credit_card_security_code")
    bank_account = factory.SubFactory(BankAccountFactory)
    money = factory.Faker("pydecimal", left_digits=3, right_digits=2, positive=True)


class MoneyTransactionFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = MoneyTransactionHistory

    from_card = factory.SubFactory(CardFactory)
    to_card = factory.SubFactory(CardFactory)
    amount = factory.Faker("pydecimal", left_digits=3, right_digits=2, positive=True)
