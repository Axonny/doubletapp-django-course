import pytest

from app.internal.models import BankAccount, Card
from app.internal.services import factories
from app.internal.services.bank_service import (
    CardNotFound,
    create_bank_account,
    create_card,
    get_bank_account,
    get_card,
    get_card_number_by_bank_account_number,
    get_card_number_by_name,
    get_cards,
)


def test_get_bank_account():
    bank_account = factories.BankAccountFactory()

    assert get_bank_account(bank_account.owner.id) == bank_account


def test_create_bank_account():
    personal_data = factories.PersonalDataFactory()
    bank_account = create_bank_account(personal_data.id)
    assert bank_account == BankAccount.objects.get(owner_id=personal_data.id)


def test_get_card():
    card = factories.CardFactory()
    assert get_card(card.bank_account.owner.id, card.number) == card


def test_create_card():
    bank_account = factories.BankAccountFactory()
    card = create_card(bank_account.owner.id)
    assert card == Card.objects.get(bank_account=bank_account)


def test_get_cards():
    bank_account = factories.BankAccountFactory()
    card1 = factories.CardFactory(bank_account=bank_account)
    card2 = factories.CardFactory(bank_account=bank_account)
    cards = get_cards(bank_account.owner.id)
    assert list(cards) == [card1, card2]


def test_get_card_number_by_name():
    bank_account = factories.BankAccountFactory()

    with pytest.raises(CardNotFound):
        get_card_number_by_name(bank_account.owner.username)

    card = factories.CardFactory(bank_account=bank_account)
    assert card.number == get_card_number_by_name(bank_account.owner.username)


def test_get_card_number_by_bank_account_number():
    bank_account = factories.BankAccountFactory()
    card = factories.CardFactory(bank_account=bank_account)
    assert card.number == get_card_number_by_bank_account_number(bank_account.number)
