import pytest
from asgiref.sync import sync_to_async

from app.internal.models import PersonalData
from app.internal.services import factories
from app.internal.services.user_service import (
    PhoneNotSetException,
    add_to_favourites,
    check_has_favourite,
    create_or_update_personal_data,
    get_favourites,
    remove_from_favourites,
    save_phone,
    try_get_user_info,
)


def test_create_personal_data():
    create_or_update_personal_data(1, "test")

    personal_data = PersonalData.objects.get(id=1)
    assert personal_data.username == "test"


def test_update_personal_data():
    create_or_update_personal_data(1, "test")
    create_or_update_personal_data(1, "test2")

    personal_data = PersonalData.objects.get(id=1)
    assert personal_data.username == "test2"


def test_get_user_info():
    personal_data = factories.PersonalDataFactory()

    assert try_get_user_info(personal_data.id) == {
        "id": personal_data.id,
        "username": personal_data.username,
        "phone": personal_data.phone,
    }


def test_get_user_info_without_phone():
    personal_data = factories.PersonalDataFactory(phone="")

    with pytest.raises(PhoneNotSetException):
        try_get_user_info(personal_data.id)


def test_save_phone():
    personal_data = factories.PersonalDataFactory(phone="")
    save_phone(personal_data.id, "+78005553535")

    personal_data = PersonalData.objects.get(id=personal_data.id)
    assert personal_data.phone == "+78005553535"


def test_add_to_favourites():
    user = factories.PersonalDataFactory()
    favourite = factories.PersonalDataFactory()

    add_to_favourites(user_id=user.id, favourite_username=favourite.username)

    favourite_db = user.favourites.get()
    assert favourite_db.username == favourite.username


def test_remove_from_favourites():
    user = factories.PersonalDataFactory()
    favourite = factories.PersonalDataFactory()
    user.favourites.add(favourite)

    remove_from_favourites(user_id=user.id, favourite_username=favourite.username)

    favourite_db = user.favourites.first()
    assert favourite_db is None


def test_get_favourites():
    user = factories.PersonalDataFactory()
    favourite = factories.PersonalDataFactory()
    user.favourites.add(favourite)

    result = get_favourites(user_id=user.id)

    assert result == [f.username for f in user.favourites.all()]


def test_check_has_favourites():
    user = factories.PersonalDataFactory()
    favourite = factories.PersonalDataFactory()
    user.favourites.add(favourite)

    assert check_has_favourite(user_id=user.id, favourite_username=favourite.username)
    assert not check_has_favourite(user_id=favourite.id, favourite_username=user.username)
