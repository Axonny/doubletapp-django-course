import datetime

import jwt
from django.conf import settings
from django.contrib.auth.hashers import check_password, make_password

from app.internal.models import PersonalData


def save_password(user_id, password):
    user = PersonalData.objects.get(id=user_id)
    user.password_hash = make_password(password, salt=settings.PASSWORD_SALT)
    user.save()


def verify_password(user: PersonalData, password: str) -> bool:
    user_password = user.password_hash
    return user_password is not None and check_password(password, user_password)


def generate_access_token(user):
    access_token_payload = {
        "sub": user.id,
        "exp": datetime.datetime.utcnow() + datetime.timedelta(days=0, minutes=5),
        "iat": datetime.datetime.utcnow(),
    }
    access_token = jwt.encode(access_token_payload, settings.SECRET_KEY, algorithm="HS256")
    return access_token


def generate_refresh_token(user):
    refresh_token_payload = {
        "sub": user.id,
        "exp": datetime.datetime.utcnow() + datetime.timedelta(days=7),
        "iat": datetime.datetime.utcnow(),
    }
    refresh_token = jwt.encode(refresh_token_payload, settings.SECRET_KEY, algorithm="HS256")

    return refresh_token
