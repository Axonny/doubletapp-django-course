from functools import wraps

from app.internal.models import PersonalData


class PhoneNotSetException(Exception):
    pass


class PersonNotFoundException(Exception):
    pass


def get_user_id_by_username(username: str) -> int:
    user = PersonalData.objects.get(username=username)
    return user.id


def create_or_update_personal_data(user_id: int, username: str) -> None:
    PersonalData.objects.update_or_create(id=user_id, defaults={"username": username})


def try_get_user_info(user_id: int) -> dict:
    user = PersonalData.objects.get(id=user_id)

    if not user.phone:
        raise PhoneNotSetException

    return {
        "id": user.id,
        "username": user.username,
        "phone": user.phone,
    }


def save_phone(user_id: int, phone: str) -> None:
    PersonalData.objects.filter(id=user_id).update(phone=phone)


def add_to_favourites(user_id: int, favourite_username: str) -> None:
    try:
        user = PersonalData.objects.get(id=user_id)
        favourite = PersonalData.objects.get(username=favourite_username)
    except PersonalData.DoesNotExist:
        raise PersonNotFoundException()
    user.favourites.add(favourite)


def remove_from_favourites(user_id: int, favourite_username: str) -> None:
    try:
        user = PersonalData.objects.get(id=user_id)
        favourite = PersonalData.objects.get(username=favourite_username)
    except PersonalData.DoesNotExist:
        raise PersonNotFoundException()
    user.favourites.remove(favourite)


def get_favourites(user_id: int) -> list:
    try:
        user = PersonalData.objects.get(id=user_id)
    except PersonalData.DoesNotExist:
        return []
    return list(user.favourites.values_list("username", flat=True))


def check_has_favourite(user_id: int, favourite_username: str) -> bool:
    try:
        user = PersonalData.objects.get(id=user_id)
    except PersonalData.DoesNotExist:
        return False
    favourite = user.favourites.filter(username=favourite_username).first()
    return bool(favourite)
