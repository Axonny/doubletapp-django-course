from decimal import Decimal
from typing import List, Optional

from django.db import IntegrityError, transaction
from django.db.models import Q
from faker import Faker

from app.internal.models import BankAccount, Card, MoneyTransactionHistory
from app.internal.services.user_service import get_user_id_by_username

fake = Faker()


class CardNotFound(Exception):
    pass


class NotEnoughMoney(Exception):
    pass


def create_bank_account(user_id: int) -> BankAccount:
    with transaction.atomic():
        bank_account = BankAccount.objects.create(number=fake.iban(), owner_id=user_id)
    return bank_account


def create_card(user_id: int) -> Card:
    bank_account = get_bank_account(user_id)

    with transaction.atomic():
        card = Card.objects.create(
            number=fake.credit_card_number(card_type="mastercard"),
            expiry=fake.credit_card_expire(),
            code=fake.credit_card_security_code(),
            bank_account=bank_account,
            money=fake.pydecimal(left_digits=3, right_digits=2, positive=True),
        )
    return card


def get_bank_account(user_id: int) -> Optional[BankAccount]:
    try:
        return BankAccount.objects.get(owner_id=user_id)
    except BankAccount.DoesNotExist:
        return None


def get_cards(user_id: int) -> List[Card]:
    bank_account = get_bank_account(user_id)
    if not bank_account:
        return []

    bank_account = BankAccount.objects.prefetch_related("cards").get(number=bank_account.number)
    return bank_account.cards.all()


def get_card(user_id: int, card_number: str) -> Optional[Card]:
    bank_account = get_bank_account(user_id)
    if not bank_account:
        return None

    try:
        return Card.objects.get(bank_account=bank_account, number=card_number)
    except Card.DoesNotExist:
        return None


def get_card_number_by_name(username: str) -> str:
    user_id = get_user_id_by_username(username)
    cards = get_cards(user_id)

    if len(cards) == 0:
        raise CardNotFound()

    return cards[0].number


def get_card_number_by_bank_account_number(number: str) -> str:
    bank_account = BankAccount.objects.get(number=number)
    return bank_account.cards.first().number


def send_money_by_card_number(user_id: int, card_number: str, amount: Decimal) -> bool:
    sender_cards = get_cards(user_id)
    recipient_card = Card.objects.get(number=card_number)

    if not recipient_card:
        raise CardNotFound()

    available_sender_cards = [card for card in sender_cards if card.money >= amount]

    if len(available_sender_cards) == 0:
        raise NotEnoughMoney()

    recipient_card = Card.objects.filter(number=card_number).first()
    if not recipient_card:
        raise CardNotFound()

    return process_money_transaction(available_sender_cards[0], recipient_card, amount)


def process_money_transaction(from_card: Card, to_card: Card, amount: Decimal) -> bool:
    try:
        with transaction.atomic():
            from_card.money -= amount
            to_card.money += amount
            from_card.save()
            to_card.save()
            MoneyTransactionHistory.objects.create(from_card=from_card, to_card=to_card, amount=amount)
    except IntegrityError:
        return False
    return True


def get_bank_account_history(user_id: int) -> list[MoneyTransactionHistory]:
    cards_ids = Card.objects.filter(bank_account__owner=user_id).values_list("number", flat=True)
    return (
        MoneyTransactionHistory.objects.filter(Q(from_card__in=cards_ids) | Q(to_card__in=cards_ids))
        .order_by("created_at")
        .all()
    )


def get_card_history(user_id: int, card_number: str) -> list[MoneyTransactionHistory]:
    return (
        MoneyTransactionHistory.objects.filter(Q(from_card__number=card_number) | Q(to_card__number=card_number))
        .order_by("created_at")
        .all()
    )


def get_user_interactions(user_id: int) -> set[str]:
    cards_ids = Card.objects.filter(bank_account__owner=user_id).values_list("number", flat=True)
    receiver_usernames = (
        MoneyTransactionHistory.objects.filter(from_card__in=cards_ids)
        .values_list("to_card__bank_account__owner__username", flat=True)
        .all()
    )
    sender_usernames = (
        MoneyTransactionHistory.objects.filter(to_card__in=cards_ids)
        .values_list("from_card__bank_account__owner__username", flat=True)
        .all()
    )

    return {*receiver_usernames, *sender_usernames}


def beautify_history(user_id: int, history_models: list[MoneyTransactionHistory]) -> str:
    user_cards_ids = Card.objects.filter(bank_account__owner=user_id).values_list("number", flat=True)
    lines = []
    for history in history_models:
        line = (
            f"[{history.created_at.strftime('%m/%d/%Y %H:%M')}]"
            f" **{history.from_card.number[-4:]} ➡️  **{history.to_card.number[-4:]}:"
            f" {'+' if history.to_card.number in user_cards_ids else '-'}{history.amount}₽"
        )

        lines.append(line)

    return "\n".join(lines)
