from enum import Enum

from app.internal.models import BotTexts


class BotText(Enum):
    saved_password = "saved_password"
    history = "history"
    history_empty = "history_empty"
    user_interactions = "user_interactions"
    interactions_empty = "interactions_empty"
    arguments_error = "arguments_error"
    bank_account_already_exists = "bank_account_already_exists"
    bank_account_created = "bank_account_created"
    bank_account_not_exists = "bank_account_not_exists"
    card_created = "card_created"
    cards_not_exists = "cards_not_exists"
    cards = "cards"
    card_not_exists = "card_not_exists"
    card = "card"
    bank_account = "bank_account"
    help = "help"
    unknown_error = "unknown_error"
    phone_not_set = "phone_not_set"
    start = "start"
    me = "me"
    button_set_phone = "button_set_phone"
    set_phone = "set_phone"
    phone_saved = "phone_saved"
    favourites_add_success = "favourites_add_success"
    favourites_remove_success = "favourites_remove_success"
    favourites_error = "favourites_error"
    favourites = "favourites"
    favourites_empty = "favourites_empty"
    user_not_in_favourites = "user_not_in_favourites"
    send_money_command_format_error = "send_money_command_format_error"
    send_money_success = "send_money_success"
    send_money_error = "send_money_error"
    not_enough_money = "not_enough_money"


def get_text(key: BotText, **kwargs) -> str:
    try:
        return BotTexts.objects.get(key=key.value).text.format(**kwargs)
    except BotTexts.DoesNotExist:
        return f"Internal error: {key.value} don't exist"
