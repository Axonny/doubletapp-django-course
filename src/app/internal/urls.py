from django.conf import settings
from django.urls import include, path
from drf_spectacular.views import SpectacularJSONAPIView, SpectacularSwaggerView
from rest_framework.routers import DefaultRouter

from app.internal.transport.rest import views

router = DefaultRouter()

router.register(r"user", views.PersonalDataApiView, basename="user")
router.register(r"login", views.LoginView, basename="login")
router.register(r"update-tokens", views.UpdateTokens, basename="update_tokens")


urlpatterns = [path("", include(router.urls)), path("", include(router.urls))]


if settings.SWAGGER:
    urlpatterns.extend(
        [
            path("schema/", SpectacularJSONAPIView.as_view(), name="schema"),
            path("schema/swagger-ui/", SpectacularSwaggerView.as_view(url_name="schema"), name="swagger-ui"),
        ]
    )
