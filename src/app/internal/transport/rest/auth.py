import jwt
from django.conf import settings
from rest_framework import exceptions
from rest_framework.authentication import BaseAuthentication

from app.internal.models import PersonalData, Token


class JWTAuthentication(BaseAuthentication):
    def authenticate(self, request):
        authorization_header = request.headers.get("Authorization")

        if not authorization_header:
            return None
        try:
            access_token = authorization_header.split(" ")[1]
            if Token.objects.filter(jti=access_token).first():
                raise exceptions.AuthenticationFailed("It's a refresh token")
            payload = jwt.decode(access_token, settings.SECRET_KEY, algorithms=["HS256"])

        except jwt.DecodeError:
            raise exceptions.AuthenticationFailed("access_token invalid")
        except jwt.ExpiredSignatureError:
            raise exceptions.AuthenticationFailed("access_token expired")
        except IndexError:
            raise exceptions.AuthenticationFailed("Token prefix missing")

        user = PersonalData.objects.filter(id=payload["sub"]).first()
        if user is None:
            raise exceptions.AuthenticationFailed("User not found")

        return user, None
