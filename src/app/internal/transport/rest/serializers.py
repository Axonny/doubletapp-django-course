from rest_framework import serializers

from app.internal.models import PersonalData


class PersonalDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = PersonalData
        fields = [
            "username",
            "phone",
        ]


class UserLoginSerializer(serializers.ModelSerializer):
    password = serializers.CharField()

    class Meta:
        model = PersonalData
        fields = [
            "username",
            "password",
        ]
