from rest_framework import generics, permissions

from app.internal.models import PersonalData
from app.internal.transport.rest.serializers import PersonalDataSerializer


class PersonalDataApiView(generics.RetrieveAPIView):
    serializer_class = PersonalDataSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = PersonalData.objects.all()
