import jwt
from django.conf import settings
from rest_framework import exceptions, mixins, permissions, viewsets
from rest_framework.response import Response

from app.internal.models import PersonalData, Token
from app.internal.services.auth_service import generate_access_token, generate_refresh_token, verify_password
from app.internal.transport.rest.serializers import PersonalDataSerializer, UserLoginSerializer


class PersonalDataApiView(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = PersonalData.objects.all()
    serializer_class = PersonalDataSerializer

    def list(self, request, *args, **kwargs):
        user = request.user
        serialized_user = self.get_serializer(user).data
        return Response({"user": serialized_user})


class LoginView(viewsets.GenericViewSet):
    queryset = PersonalData.objects.all()
    serializer_class = UserLoginSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request, *args, **kwargs):
        username = request.data.get("username")
        password = request.data.get("password")
        response = Response()
        if (username is None) or (password is None):
            raise exceptions.AuthenticationFailed("username and password required")

        user = self.queryset.filter(username=username).first()
        if user is None:
            raise exceptions.AuthenticationFailed("user not found")
        if not verify_password(user, password):
            raise exceptions.AuthenticationFailed("wrong password")

        serialized_user = PersonalDataSerializer(user).data

        access_token = generate_access_token(user)
        refresh_token = generate_refresh_token(user)
        Token.objects.create(user=user, jti=refresh_token)

        response.set_cookie(key="refreshtoken", value=refresh_token, httponly=True)
        response.data = {
            "access_token": access_token,
            "refresh_token": refresh_token,
            "user": serialized_user,
        }

        return response


class UpdateTokens(viewsets.GenericViewSet):
    queryset = PersonalData.objects.all()
    serializer_class = UserLoginSerializer
    permission_classes = [permissions.AllowAny]

    def create(self, request, *args, **kwargs):
        refresh_token = request.COOKIES.get("refreshtoken")
        response = Response()
        if refresh_token is None:
            raise exceptions.AuthenticationFailed("Authentication credentials were not provided.")

        try:
            payload = jwt.decode(refresh_token, settings.SECRET_KEY, algorithms=["HS256"])
        except jwt.DecodeError:
            raise exceptions.AuthenticationFailed("refresh_token invalid")
        except jwt.ExpiredSignatureError:
            raise exceptions.AuthenticationFailed("refresh_token expired")

        user = PersonalData.objects.filter(id=payload["sub"]).first()
        if user is None:
            raise exceptions.AuthenticationFailed("User not found")

        token = Token.objects.filter(user=user, jti=refresh_token)
        if not token.exists() or token.first().revoked:
            raise exceptions.AuthenticationFailed("Token not found")

        access_token = generate_access_token(user)
        new_refresh_token = generate_refresh_token(user)

        token.update(revoked=True)
        Token.objects.create(user=user, jti=new_refresh_token)
        response.set_cookie(key="refreshtoken", value=new_refresh_token, httponly=True)

        response.data = {
            "access_token": access_token,
            "refresh_token": new_refresh_token,
        }
        return response
