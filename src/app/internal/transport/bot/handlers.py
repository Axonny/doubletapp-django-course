from decimal import Decimal
from functools import wraps

from django.conf import settings
from telegram import KeyboardButton, ReplyKeyboardMarkup, Update
from telegram.ext import CallbackContext

from app.internal.models import BankAccount, Card, PersonalData
from app.internal.services import auth_service, bank_service, user_service
from app.internal.services.bot_text_service import BotText, get_text
from app.internal.services.user_service import create_or_update_personal_data, save_phone, try_get_user_info


def need_phone(func: callable):
    @wraps(func)
    def wrapper(*args, **kwargs):
        update = kwargs.get("update") or args[0]
        user = PersonalData.objects.get(id=update.effective_user.id)
        if not user.phone:
            update.message.reply_text(text=get_text(BotText.phone_not_set))
            return None
        return func(*args, **kwargs)

    return wrapper


def need_bank_account(func: callable):
    @wraps(func)
    def wrapper(*args, **kwargs):
        update = kwargs.get("update") or args[0]
        bank_account = BankAccount.objects.filter(owner_id=update.effective_user.id).first()
        if not bank_account:
            update.message.reply_text(text=get_text(BotText.bank_account_not_exists))
            return None
        return func(*args, **kwargs)

    return wrapper


def need_card(func: callable):
    @wraps(func)
    def wrapper(*args, **kwargs):
        update = kwargs.get("update") or args[0]
        bank_account = BankAccount.objects.filter(owner_id=update.effective_user.id).first()
        card = Card.objects.filter(bank_account=bank_account).first()
        if not card:
            update.message.reply_text(text=get_text(BotText.cards_not_exists))
            return None
        return func(*args, **kwargs)

    return wrapper


def handle_error(update, context: CallbackContext) -> None:
    if settings.DEBUG:
        import traceback

        print(str(context.error) + "\n\n" + traceback.format_exc())


def start(update: Update, context: CallbackContext) -> None:
    create_or_update_personal_data(update.effective_user.id, update.effective_user.username)
    update.message.reply_text(text=get_text(BotText.start))


@need_phone
def me(update: Update, context: CallbackContext) -> None:
    user_info = try_get_user_info(update.effective_user.id)

    update.message.reply_text(
        text=get_text(BotText.me, id=user_info["id"], username=user_info["username"], phone=user_info["phone"])
    )


def set_phone(update: Update, context: CallbackContext) -> None:
    reply_markup = ReplyKeyboardMarkup(
        [[KeyboardButton(text=get_text(BotText.button_set_phone), request_contact=True)]], one_time_keyboard=True
    )
    update.message.reply_text(text=get_text(BotText.set_phone), reply_markup=reply_markup)


def set_contact(update: Update, context: CallbackContext) -> None:
    save_phone(update.effective_user.id, update.effective_message.contact.phone_number)
    update.message.reply_text(text=get_text(BotText.phone_saved))


def get_help(update: Update, context: CallbackContext) -> None:
    update.message.reply_text(text=get_text(BotText.help))


@need_phone
def create_bank_account(update: Update, context: CallbackContext) -> None:
    if bank_service.get_bank_account(update.effective_user.id):
        update.message.reply_text(text=get_text(BotText.bank_account_already_exists))
        return
    bank_service.create_bank_account(update.effective_user.id)
    update.message.reply_text(text=get_text(BotText.bank_account_created))


@need_phone
@need_bank_account
def create_card(update: Update, context: CallbackContext) -> None:
    bank_service.create_card(update.effective_user.id)
    update.message.reply_text(text=get_text(BotText.card_created))


@need_phone
@need_bank_account
@need_card
def get_cards(update: Update, context: CallbackContext) -> None:
    cards = bank_service.get_cards(update.effective_user.id)
    update.message.reply_text(
        text=get_text(BotText.cards, cards="\n".join([f"{card.number}: {card.expiry}" for card in cards]))
    )


@need_phone
@need_bank_account
def get_bank_account(update: Update, context: CallbackContext) -> None:
    bank_account = bank_service.get_bank_account(update.effective_user.id)
    update.message.reply_text(text=get_text(BotText.bank_account, number=bank_account.number, money=bank_account.money))


@need_phone
@need_bank_account
@need_card
def get_card_info(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(text=get_text(BotText.arguments_error))
        return

    card_number = context.args[0]
    card = bank_service.get_card(update.effective_user.id, card_number)
    if not card:
        update.message.reply_text(text=get_text(BotText.card_not_exists, card_number=card_number))
        return
    update.message.reply_text(
        text=get_text(BotText.card, number=card.number, expiry=card.expiry, security_code=card.code, money=card.money)
    )


@need_phone
def add_to_favourites(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(text=get_text(BotText.arguments_error))
        return

    try:
        user_service.add_to_favourites(update.effective_user.id, context.args[0])
    except user_service.PersonNotFoundException:
        update.message.reply_text(text=get_text(BotText.favourites_error))
        return
    update.message.reply_text(text=get_text(BotText.favourites_add_success))


@need_phone
def remove_from_favourites(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(text=get_text(BotText.arguments_error))
        return
    try:
        user_service.remove_from_favourites(update.effective_user.id, context.args[0])
    except user_service.PersonNotFoundException:
        update.message.reply_text(text=get_text(BotText.favourites_error))
        return

    update.message.reply_text(text=get_text(BotText.favourites_remove_success))


@need_phone
def get_favourites(update: Update, context: CallbackContext) -> None:
    favourites = user_service.get_favourites(update.effective_user.id)
    if not favourites:
        update.message.reply_text(text=get_text(BotText.favourites_empty))
        return
    update.message.reply_text(text=get_text(BotText.favourites, favourites="\n".join(favourites)))


def send_money(chat_id: int, bot, card_number: str, amount: Decimal) -> None:
    if not card_number:
        bot.reply_text(text=get_text(BotText.card_not_exists, card_number=card_number))
        return

    try:
        if amount > 0 and bank_service.send_money_by_card_number(chat_id, card_number, amount):
            bot.reply_text(text=get_text(BotText.send_money_success))
        else:
            bot.reply_text(text=get_text(BotText.send_money_error))
    except bank_service.NotEnoughMoney:
        bot.reply_text(text=get_text(BotText.not_enough_money))
    except bank_service.CardNotFound:
        bot.reply_text(text=get_text(BotText.card_not_exists))


@need_phone
@need_bank_account
@need_card
def send_money_by_name(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 2:
        update.message.reply_text(text=get_text(BotText.send_money_command_format_error))
        return

    if not user_service.check_has_favourite(update.effective_chat.id, context.args[0]):
        update.message.reply_text(text=get_text(BotText.user_not_in_favourites))
        return

    try:
        card_number = bank_service.get_card_number_by_name(context.args[0])
    except bank_service.CardNotFound:
        update.message.reply_text(text=get_text(BotText.card_not_exists))
        return
    send_money(update.effective_chat.id, update.message, card_number, Decimal(context.args[1]))


@need_phone
@need_bank_account
@need_card
def send_money_by_card_number(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 2:
        update.message.reply_text(text=get_text(BotText.send_money_command_format_error))
        return

    send_money(update.effective_chat.id, update.message, context.args[0], Decimal(context.args[1]))


@need_phone
@need_bank_account
@need_card
def send_money_by_bank_account_number(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 2:
        update.message.reply_text(text=get_text(BotText.send_money_command_format_error))
        return

    card_number = bank_service.get_card_number_by_bank_account_number(context.args[0])
    send_money(update.effective_chat.id, update.message, card_number, Decimal(context.args[1]))


@need_phone
@need_bank_account
@need_card
def get_user_interactions(update: Update, context: CallbackContext) -> None:
    usernames = bank_service.get_user_interactions(update.effective_user.id)
    if len(usernames) == 0:
        update.message.reply_text(text=get_text(BotText.interactions_empty))
    update.message.reply_text(text=get_text(BotText.user_interactions, usernames="\n".join(usernames)))


@need_phone
@need_bank_account
@need_card
def get_bank_account_history(update: Update, context: CallbackContext) -> None:
    history_models = bank_service.get_bank_account_history(update.effective_user.id)
    if len(history_models) == 0:
        update.message.reply_text(text=get_text(BotText.history_empty))
    history = bank_service.beautify_history(update.effective_user.id, history_models)
    update.message.reply_text(text=get_text(BotText.history, history=history))


@need_phone
@need_bank_account
@need_card
def get_card_history(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(text=get_text(BotText.send_money_command_format_error))
        return
    history_models = bank_service.get_card_history(update.effective_user.id, context.args[0])
    if len(history_models) == 0:
        update.message.reply_text(text=get_text(BotText.history_empty))
        return
    history = bank_service.beautify_history(update.effective_user.id, history_models)
    update.message.reply_text(text=get_text(BotText.history, history=history))


@need_phone
def set_password(update: Update, context: CallbackContext) -> None:
    if len(context.args) != 1:
        update.message.reply_text(text=get_text(BotText.arguments_error))
        return

    auth_service.save_password(update.effective_user.id, context.args[0])
    update.message.reply_text(text=get_text(BotText.saved_password))


handlers = {
    "start": start,
    "me": me,
    "set_phone": set_phone,
    "get_bank_account": get_bank_account,
    "get_cards": get_cards,
    "get_card": get_card_info,
    "create_bank_account": create_bank_account,
    "create_card": create_card,
    "add_to_favourites": add_to_favourites,
    "remove_from_favourites": remove_from_favourites,
    "get_favourites": get_favourites,
    "send_money_by_name": send_money_by_name,
    "send_money_by_card_number": send_money_by_card_number,
    "send_money_by_bank_account": send_money_by_bank_account_number,
    "get_user_interactions": get_user_interactions,
    "get_history": get_bank_account_history,
    "get_card_history": get_card_history,
    "set_password": set_password,
    "help": get_help,
}
