from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class PersonalData(models.Model):
    class Meta:
        db_table = "personal_data"
        verbose_name = "Пользовательские данные"
        verbose_name_plural = "Пользовательские данные"

    username = models.CharField(max_length=255, unique=True, null=True)
    phone = PhoneNumberField(region="RU", null=True)
    favourites = models.ManyToManyField("self", symmetrical=False)
    password_hash = models.CharField(max_length=128, default="", blank=True)

    def __str__(self):
        return self.username

    @property
    def is_authenticated(self):
        return True
