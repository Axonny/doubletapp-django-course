import pytest
from django.core.exceptions import ValidationError

from app.internal.models import PersonalData


def test_phone_validator_raise():
    personal_data = PersonalData(username="test01", phone="aaa")
    with pytest.raises(ValidationError):
        personal_data.full_clean()


def test_phone_validator_formats():
    personal_data = PersonalData(username="A", phone="8-800-555-35-35")
    personal_data.full_clean()
    assert personal_data.phone == "+78005553535"

    personal_data = PersonalData(username="B", phone="8 (800) 555-35-35")
    personal_data.full_clean()
    assert personal_data.phone == "+78005553535"

    personal_data = PersonalData(username="C", phone="+7 (800) 555-35-35")
    personal_data.full_clean()
    assert personal_data.phone == "+78005553535"

    personal_data = PersonalData(username="D", phone="+78005553535")
    personal_data.full_clean()
    assert personal_data.phone == "+78005553535"
