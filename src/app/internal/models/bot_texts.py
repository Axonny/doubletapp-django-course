from django.db import models


class BotTexts(models.Model):
    class Meta:
        db_table = "bot_texts"
        verbose_name = "Текст бота"
        verbose_name_plural = "Тексты бота"

    key = models.CharField(max_length=255, primary_key=True, verbose_name="Ключ")
    text = models.TextField(verbose_name="Текст")
