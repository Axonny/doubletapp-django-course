from django.db import models

from app.internal.models.personal_data import PersonalData


class Token(models.Model):
    class Meta:
        db_table = "refresh_tokens"
        verbose_name = "Токен"
        verbose_name_plural = "Токены"

    jti = models.CharField(primary_key=True)
    user = models.ForeignKey(PersonalData, related_name="refresh_tokens", on_delete=models.CASCADE)
    revoked = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
