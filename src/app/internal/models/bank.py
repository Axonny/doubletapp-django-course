from decimal import Decimal

from creditcards.models import CardExpiryField, CardNumberField, SecurityCodeField
from django.core.validators import MinValueValidator
from django.db import models


class BankAccount(models.Model):
    class Meta:
        db_table = "bank_account"
        verbose_name = "Банковский счет"
        verbose_name_plural = "Банковские счета"

    number = models.CharField(max_length=255, verbose_name="Номер счета", primary_key=True)
    owner = models.OneToOneField(
        "PersonalData", on_delete=models.PROTECT, verbose_name="Владелец", related_name="bank_account"
    )

    @property
    def money(self):
        return sum([card.money for card in self.cards.all()])


class Card(models.Model):
    class Meta:
        db_table = "card"
        verbose_name = "Карта"
        verbose_name_plural = "Карты"

    number = CardNumberField(verbose_name="Номер карты", primary_key=True)
    expiry = CardExpiryField(verbose_name="Срок действия")
    code = SecurityCodeField(verbose_name="Код безопасности")

    bank_account = models.ForeignKey("BankAccount", on_delete=models.PROTECT, verbose_name="Счет", related_name="cards")
    money = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Деньги", validators=[MinValueValidator(Decimal("0.01"))]
    )


class MoneyTransactionHistory(models.Model):
    class Meta:
        db_table = "money_transaction_history"
        verbose_name = "запись истории перевода"
        verbose_name_plural = "Записи истории переводов"
        ordering = ["created_at"]

    from_card = models.ForeignKey(
        Card, related_name="send_money_transactions", verbose_name="Карта отправителя", on_delete=models.CASCADE
    )
    to_card = models.ForeignKey(
        Card, related_name="receive_money_transactions", verbose_name="Карта получателя", on_delete=models.CASCADE
    )
    amount = models.DecimalField(
        max_digits=10, decimal_places=2, verbose_name="Сумма", validators=[MinValueValidator(Decimal("0.01"))]
    )
    created_at = models.DateTimeField(auto_now_add=True)
