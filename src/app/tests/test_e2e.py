from unittest import mock

from app.internal.services.bot_text_service import BotText, get_text
from app.internal.services.factories import PersonalDataFactory
from app.internal.transport.bot.handlers import (
    add_to_favourites,
    create_bank_account,
    create_card,
    get_bank_account,
    get_card_info,
    get_cards,
    get_favourites,
    me,
    remove_from_favourites,
    set_contact,
    set_phone,
)


def test_save_phone_and_get_me_scenario(telegram_update, telegram_context, user_without_phone):
    telegram_update = telegram_update(user_without_phone)

    me(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.phone_not_set))

    set_phone(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.set_phone), reply_markup=mock.ANY)

    telegram_update.effective_message.contact.phone_number = "+78005553535"
    set_contact(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.phone_saved))

    me(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(
        text=get_text(BotText.me, id=12345678, username="test", phone="+78005553535")
    )


def test_bank_account_scenario(telegram_update, telegram_context, user_with_phone):
    telegram_update = telegram_update(user_with_phone)

    get_bank_account(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.bank_account_not_exists))

    create_bank_account(telegram_update, telegram_context)
    bank_account = user_with_phone.bank_account
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.bank_account_created))

    create_bank_account(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.bank_account_already_exists))

    get_bank_account(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(
        text=get_text(BotText.bank_account, number=bank_account.number, money=bank_account.money)
    )


def test_cards_scenario(telegram_update, telegram_context, user_with_phone, bank_account):
    telegram_update = telegram_update(user_with_phone)

    get_cards(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.cards_not_exists))

    create_card(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.card_created))

    create_card(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.card_created))
    cards = user_with_phone.bank_account.cards.all()

    get_cards(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(
        text=get_text(BotText.cards, cards="\n".join([f"{card.number}: {card.expiry}" for card in cards]))
    )

    card = cards[0]
    telegram_context.args = [card.number]
    get_card_info(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(
        text=get_text(BotText.card, number=card.number, expiry=card.expiry, security_code=card.code, money=card.money)
    )

    telegram_context.args = []
    get_card_info(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.arguments_error))

    telegram_context.args = ["invalid_card_number"]
    get_card_info(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(
        text=get_text(BotText.card_not_exists, card_number="invalid_card_number")
    )


def test_favourite_scenario(telegram_update, telegram_context, user_with_phone):
    telegram_update = telegram_update(user_with_phone)
    favourite = PersonalDataFactory()

    add_to_favourites(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.arguments_error))

    telegram_context.args = ["invalid_name"]
    add_to_favourites(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.favourites_error))

    get_favourites(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.favourites_empty))

    telegram_context.args = [favourite.username]
    add_to_favourites(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.favourites_add_success))

    get_favourites(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(
        text=get_text(BotText.favourites, favourites=favourite.username)
    )

    telegram_context.args = [favourite.username]
    remove_from_favourites(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.favourites_remove_success))

    telegram_context.args = ["invalid_name"]
    remove_from_favourites(telegram_update, telegram_context)
    telegram_update.message.reply_text.assert_called_with(text=get_text(BotText.favourites_error))
