from http import HTTPStatus

import pytest
from django.urls import reverse


@pytest.mark.smoke
def test_api(api_client):
    response = api_client.get(reverse("user-list"))
    assert response.status_code == HTTPStatus.FORBIDDEN
