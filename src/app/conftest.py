from unittest.mock import MagicMock, patch

import pytest
from rest_framework.test import APIClient

from app.internal.services.factories import BankAccountFactory, PersonalDataFactory


def pytest_collection_modifyitems(items):
    for item in items:
        item.add_marker(pytest.mark.django_db)
        item.add_marker(pytest.mark.asyncio)


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def user_without_phone():
    return PersonalDataFactory(id=12345678, username="test", phone=None)


@pytest.fixture
def user_with_phone():
    return PersonalDataFactory(id=12345678, username="test", phone="+78005553535")


@pytest.fixture
def bank_account(user_with_phone):
    return BankAccountFactory(owner=user_with_phone)


@pytest.fixture
def telegram_update():
    update = MagicMock()

    def set_user(user):
        update.effective_user.id = user.id
        update.effective_user.username = user.username
        return update

    return set_user


@pytest.fixture
def telegram_context() -> MagicMock:
    context = MagicMock()
    return context
