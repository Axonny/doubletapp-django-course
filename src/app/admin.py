from django.contrib import admin

from app.internal.admin.admin_user import AdminUserAdmin

from .models import BankAccount, BotTexts, Card, PersonalData

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"

admin.site.register(PersonalData)
admin.site.register(BotTexts)
admin.site.register(BankAccount)
admin.site.register(Card)
