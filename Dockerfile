FROM python:3.10-slim-buster


ENV \
  # python:
  PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONHASHSEED=random \
  # pip:
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  # poetry:
  POETRY_VERSION=1.4.0 \
  POETRY_VIRTUALENVS_CREATE=false \
  POETRY_CACHE_DIR='/var/cache/pypoetry'


RUN apt-get update && apt-get install -y --no-install-recommends \
  git-core \
  make \
  && apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/* \
  && pip install "poetry==$POETRY_VERSION"


WORKDIR /app
COPY ./poetry.lock ./pyproject.toml /app/
RUN poetry install --no-interaction --no-ansi

COPY ./Makefile /app/
COPY ./src/ /app/

