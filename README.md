# DoubleTap Django Course

Команды для запуска проекта:

```bash
make app # запуск проекта локально
make bot # запуск бота локально

make run_app # запуск проекта в докере
make run_bot # запуск бота в докере

make stop_app # остановка проекта в докере
make stop_bot # остановка бота в докере
```



